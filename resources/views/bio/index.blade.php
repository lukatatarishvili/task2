@extends('layouts.app')

@section('content')
<h1 style="text-align: center">Bios</h1>
<a href="bio/create" class="btn btn-primary" style="margin-bottom: 15px;">Create post</a>

    <div class="card">
        
                
        

        <table id="posts-table" class="table table-hover posts-table">
            <thead>
              <tr>
                <th scope="col">#</th>
                
                <th scope="col">First name</th>
                <th scope="col">Last name</th>
                <th scope="col">Address</th>
                <th scope="col">Bio</th>
                <th scope="col">Birth date</th>
                <th scope="col">Created_at</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($bios as $bio)
                <tr>
                    <td> {{$bio->id}}</td>
                    <td> {{$bio->first_name}}</td>
                    <td> {{$bio->last_name}}</td>
                    <td> {{$bio->address}}</td>
                    <td> {{$bio->bio}}</td>
                    <td> {{$bio->birth_date}}</td>
                    <td> {{$bio->created_at}}</td>

                    @csrf
                    
                    
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
                
    </div>
        
    
@endsection
