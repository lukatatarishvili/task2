@extends('layouts.app')

@section('content')



<h1 style="text-align: center">Add bio</h1>

        <form method="post" action="{{ action("BiosController@store") }}">
            <div class="form-group">
                <label for="first_name">Firs tname</label>
                <input type="text" name="first_name" id="first_name" class="form-control" id="">
            </div>
            <div class="form-group">
                <label for="last_name">last name</label>
                <input type="text" name="last_name" id="last_name" class="form-control" id="">
            </div>
            <div class="form-group">
                <label for="address">address</label>
                <input type="text" name="address" id="address" class="form-control" id="">
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <textarea name="bio" id="bio" cols="30" class="form-control" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="birth_date">birth date</label>
                <input type="date" name="birth_date" id="birth_date" class="form-control" id="">
            </div>

            <input type="submit" class="btn btn-primary" value="submit">
            @csrf
        </form>
    
        
@endsection